from tornado import websocket
import tornado.ioloop
import threading
import time
import random
from random import randint

class TempSocket(websocket.WebSocketHandler):
    clients = []
    def check_origin(self, origin):
        return True

    def open(self):
        print('Socket Opened')
        TempSocket.clients.append(self)

    def on_close(self):
        print('Socket Closed')

    @classmethod
    def write_to_clients(cls, message):
        for client in cls.clients:
            client.write_message(message)

class PressureSocket(websocket.WebSocketHandler):
    clients = []
    def check_origin(self, origin):
        return True

    def open(self):
        print('Socket Opened')
        PressureSocket.clients.append(self)

    def on_close(self):
        print('Socket Closed')

    @classmethod
    def write_to_clients(cls, message):
        for client in cls.clients:
            client.write_message(message)

class CycleSocket(websocket.WebSocketHandler):
    clients = []
    def check_origin(self, origin):
        return True

    def open(self):
        print('Socket Opened')
        CycleSocket.clients.append(self)

    def on_close(self):
        print('Socket Closed')

    @classmethod
    def write_to_clients(cls, message):
        for client in cls.clients:
            client.write_message(message)

# Return a new value based on original INPUT plus or minus the RANGE
def plusOrMinus(input, range):
    return input + ((-1)**random.randrange(2) * randint(1, range))

def main():
    print("Starting Main Method")
    application = tornado.web.Application([(r"/pressure", PressureSocket), (r"/temp", TempSocket), (r"/cycle", CycleSocket)], debug=False)
    application.listen(9000)
    t3 = threading.Thread(target=tornado.ioloop.IOLoop.instance().start)
    t3.setDaemon(True)
    t3.start()
    print("Started Thread for Tornado")
    cycles = 0
    a_temp_gun = 50
    b_temp_gun = 50
    a_temp_in = 40
    b_temp_in = 35
    a_temp_out = 29
    b_temp_out = 28
    a_press_gun = 200
    b_press_gun = 180
    a_press_in = 400
    b_press_in = 400
    a_press_out = 1000
    b_press_out = 1000
    while True:
        a_temp_gun = plusOrMinus(a_temp_gun, 1)
        b_temp_gun = plusOrMinus(b_temp_gun, 1)
        a_temp_in = plusOrMinus(a_temp_in, 1)
        b_temp_in = plusOrMinus(b_temp_in, 1)
        a_temp_out = plusOrMinus(a_temp_out, 1)
        b_temp_out = plusOrMinus(b_temp_out, 1)
        a_press_gun = plusOrMinus(a_press_gun, 1)
        b_press_gun = plusOrMinus(b_press_gun, 1)
        a_press_in = plusOrMinus(a_press_in, 1)
        b_press_in = plusOrMinus(a_press_in, 10)
        a_press_out = plusOrMinus(a_press_in, 10)
        b_press_out = plusOrMinus(a_press_in, 10)
        TempSocket.write_to_clients("*,{0},{1},{2},{3},{4},{5}".format(a_temp_gun, b_temp_gun, a_temp_in, b_temp_in, a_temp_out, b_temp_out))
        PressureSocket.write_to_clients("$,{0},{1},{2},{3},{4},{5}".format(a_press_gun, b_press_gun, a_press_in, b_press_in, a_press_out, b_press_out))
        CycleSocket.write_to_clients("!,{0}".format(cycles))
        cycles += 1
        time.sleep(0.25)


if __name__ == "__main__":
    print("Staring App Main")
    main()