import angular from 'angular';

import '../style/app.css';

let app = () => {
  return {
    template: require('./app.html'),
    controller: 'AppCtrl',
    controllerAs: 'app'
  }
};

class AppCtrl {
  constructor($scope) {
    'ngInject'

    this.unit = { friendlyName: "Unit Friendly Name" };
    

    function monthFilter(input) {
      const months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
      return months[input];
    }

    let timeString = "";
    let date = new Date();
    timeString = `${date.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })} ${monthFilter(date.getMonth())} ${date.getDay()}, ${date.getFullYear()}`;
    this.time = timeString;

    this.gun = { aTemp: 0, bTemp: 0, aPress: 0, bPress: 0, pressDelta: 0 };
    this.inlet = { aTemp: 0, bTemp: 0, aPress: 0, bPress: 0, pressDelta: 0 };
    this.outlet = { aTemp: 0, bTemp: 0, aPress: 0, bPress: 0, pressDelta: 0 };
    this.cycles = 0;

    let wsPress = new WebSocket("ws://localhost:9000/pressure");
    let wsTemp = new WebSocket("ws://localhost:9000/temp");
    let wsCycle = new WebSocket("ws://localhost:9000/cycle");

    wsPress.onmessage = (evt) => {
        this.message = "Socket open";
        this.gun.aPress = evt.data.split(",")[1];
        this.gun.bPress = evt.data.split(",")[2];
        this.inlet.aPress = evt.data.split(",")[3];
        this.inlet.bPress = evt.data.split(",")[4]; 
        this.outlet.aPress = evt.data.split(",")[5];
        this.outlet.bPress = evt.data.split(",")[6]; 
        this.gun.pressDelta = Math.abs(this.gun.aPress - this.gun.bPress);
        this.inlet.pressDelta = Math.abs(this.inlet.aPress - this.inlet.bPress);
        this.outlet.pressDelta = Math.abs(this.outlet.aPress - this.outlet.bPress);
        $scope.$apply();
    };

    wsTemp.onmessage = (evt) => {
      this.message = "Socket open";
      this.gun.aTemp = evt.data.split(",")[1];
      this.gun.bTemp = evt.data.split(",")[2];
      this.inlet.aTemp = evt.data.split(",")[3];
      this.inlet.bTemp = evt.data.split(",")[4]; 
      this.outlet.aTemp = evt.data.split(",")[5];
      this.outlet.bTemp = evt.data.split(",")[6]; 
      this.gun.tempDelta = Math.abs(this.gun.aTemp - this.gun.bTemp);
      this.inlet.tempDelta = Math.abs(this.inlet.aTemp - this.inlet.bTemp);
      this.outlet.tempDelta = Math.abs(this.outlet.aTemp - this.outlet.bTemp);
      $scope.$apply();
  };


    wsCycle.onmessage = (evt) => {
        this.cycles = evt.data.split(",")[1];
        $scope.$apply();
    };

    wsPress.onopen = function(evt) { onOpen(evt) };
    wsPress.onclose = function(evt) { onClose(evt) };
    wsPress.onerror = function(evt) { onError(evt) };

    function onOpen(evt) {
      console.log(evt);
    }
    function onClose(evt) {
      console.log(evt.code);
      console.log(evt);
    }
    function onError(evt) {
      console.log(evt);
    }
  }
}

const MODULE_NAME = 'app';

angular.module(MODULE_NAME, [])
  .directive('app', app)
  .controller('AppCtrl', AppCtrl);

export default MODULE_NAME;